# Pull base image
FROM centos:latest

# Install Updates
RUN yum install -y java-1.8.0-openjdk.x86_64

# Install/Run Bamboo 
RUN mkdir /srv/bamboo
WORKDIR /srv/bamboo/ 
COPY bamboo-agent-5.10-OD-15-001.jar /srv/bamboo/
CMD /usr/bin/java -jar bamboo-agent-5.10-OD-15-001.jar
